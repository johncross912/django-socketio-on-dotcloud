#!/usr/local/env python

from datetime import datetime
import json
import time


# Open the environment file and get the name of the host and port
with open('/home/dotcloud/environment.json') as f:
    env = json.load(f)


while True:
    # Give todays date as an item in a dictionary.
    today = { 'date and time' : datetime.today() }
    print today

    time.sleep(60)