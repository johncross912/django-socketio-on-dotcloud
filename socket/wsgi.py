import os
import sys

from django.core.handlers.wsgi import WSGIHandler

from socketio.server import SocketIOServer

PORT = 8080

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__),'hellodjango')))
os.environ['DJANGO_SETTINGS_MODULE'] = 'hellodjango.settings'

# Make the port available here for the path:
#   socketio_tags.socketio ->
#   socketio_scripts.html ->
#   io.Socket JS constructor
# allowing the port to be set as the client-side default there.
os.environ["DJANGO_SOCKETIO_PORT"] = str(PORT)


bind = ('127.0.0.1', int(PORT))
print
print "SocketIOServer running on %s:%s" % bind
print
print "Socket.io env: {0}".format(os.environ.get("socketio", 'Empty'))

handler = WSGIHandler()
server = SocketIOServer(bind, handler, resource="socket.io")
server.serve_forever()
print "Socket.io env: {0}".format(os.environ.get("socketio", 'Empty'))